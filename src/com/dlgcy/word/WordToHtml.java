package com.dlgcy.word;
import java.io.File;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class WordToHtml
{
	//-------------------------------------------------
	//方法原型: change(String paths)
	//功能描述:
	//将指定目录下面所有的doc文件转化为HTML
	//输入参数: String
	//输出参数: 无
	//返 回 值: 无
	//其它说明: 递归
	//-------------------------------------------- 
	public static void change(String paths, String savepaths){ 

		File d = new File(paths);
		//取得当前文件夹下所有文件和目录的列表
		File lists[] = d.listFiles();
		String pathss = new String("");

		//对当前目录下面所有文件进行检索
		for(int i = 0; i < lists.length; i ++)
		{
			if(lists[i].isFile())
			{
				String filename = lists[i].getName();
				String filetype = new String("");
				//取得文件类型
				filetype = filename.substring((filename.length() - 3), filename.length());

				//判断是否为doc文件
				if(filetype.equals("doc"))
				{
					System.out.println("当前正在转换......");
					//打印当前目录路径
					System.out.println(paths);
					//打印doc文件名
					System.out.println(filename.substring(0, (filename.length() - 4)));

					ActiveXComponent app = new ActiveXComponent("Word.Application");//启动word

					String docpath = paths + filename;
					String htmlpath = savepaths + filename.substring(0, (filename.length() - 4));

					String inFile = docpath;
					//要转换的word文件
					String tpFile = htmlpath;
					//HTML文件

					WordToHtml.wordToHtml(inFile, tpFile);
				}
			}
			else{
				pathss = paths;
				//进入下一级目录
				pathss = pathss + lists[i].getName() + "\\";    
				//递归遍历所有目录
				change(pathss, savepaths);
			}
		}
	} 
	
	/*
    * 将指定路径的Word文件生成Html文件
    * 
    * @param filename
    *            word文件存放路径及文件名,可以是网络地址
    * @param savefilename
    *            生成后的html存放路径及文件名
    */
   public static void wordToHtml(String filename, String savefilename)
   {
       ActiveXComponent app = new ActiveXComponent("Word.Application");// 启动word
       try{
           app.setProperty("Visible", new Variant(false));
           // 设置word不可见
           Dispatch docs = app.getProperty("Documents").toDispatch();
           Dispatch doc = Dispatch.invoke(docs, "Open", Dispatch.Method, new Object[] { filename, new Variant(false), new Variant(true) }, new int[1]).toDispatch();
           // 打开word文件
           Dispatch.invoke(doc, "SaveAs", Dispatch.Method, new Object[] { savefilename, new Variant(8) }, new int[1]);
           // 作为html格式保存到临时文件
           Dispatch.call(doc, "Close", new Variant(false));
       }
       catch (Exception e){
           e.printStackTrace();
       }
       finally{
           app.invoke("Quit", new Variant[] {});
           app = null;
       }
       System.out.println("转化完毕！");
   }


	public static void main(String[] args)
	{

		String paths = new String("D:\\Work\\");
		String savepaths = new String ("D:\\Work\\"); 
		change(paths, savepaths);

	}
}

