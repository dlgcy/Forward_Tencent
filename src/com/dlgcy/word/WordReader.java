package com.dlgcy.word;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.POIXMLDocument;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class WordReader { 
	public WordReader(){ 
	} 
	/** 
	 * @param filePath 文件路径 
	 * @return 读出的Word的内容 
	 */ 
	public static String getTextFromWord(String filePath){ 
		String result = null; 
		File file = new File(filePath); 
		try{ 
			OPCPackage oPCPackage = POIXMLDocument.openPackage(filePath);
			XWPFDocument xwpf = new XWPFDocument(oPCPackage);
			POIXMLTextExtractor ex = new XWPFWordExtractor(xwpf);
			result = ex.getText();
			oPCPackage.close();
			System.out.println("读取了word2007以上（含）格式的内容。");
		}catch(FileNotFoundException e){ 
			e.printStackTrace(); 
		}catch(IOException e){ 
			e.printStackTrace(); 
			try {
				FileInputStream fis = new FileInputStream(file); 
				WordExtractor wordExtractor = new WordExtractor(fis); 
				result = wordExtractor.getText();
				fis.close();
				System.out.println("读取了word2003格式的内容。");
			} catch (FileNotFoundException e1) {
				// TODO 自动生成的 catch 块
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO 自动生成的 catch 块
				e1.printStackTrace();
			} 
		}
		return result; 
	} 
} 
