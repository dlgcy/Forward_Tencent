package com.dlgcy.weibo;

import java.io.File;
import java.util.ArrayList;
import com.dlgcy.word.StringUtil;

public class DataProcessor {
	//ArrayList<String[]> finalStrArray = new ArrayList<String[]>();
	public String[][] strArray;  //最终文字数据二维数组；
	public int strCount, picCount;  //文字和链接、图片数；
	String PIC_PART_NAME = "image";
	public String[] picArray;  //图片路径数组；
	String PIC_PATH = ConfigUtil.getValue("WORK_SPACE") + "html.files";

	public DataProcessor(String originalStr){
		orderStrings(originalStr);
		findPictures(PIC_PATH);
	}

	public void orderStrings(String originalStr){

		String splitStr = "http://";		
		String[] tempStrArray ;	
		originalStr.replaceAll( "\\s", "" );
		originalStr = StringUtil.replaceBlank(originalStr);
		tempStrArray = originalStr.split(splitStr);
		strCount = tempStrArray.length;
		//System.out.println(count);  //比预想的多一个("")；
		strArray = new String[strCount][2];

		int j = 0;
		for(String s : tempStrArray){
			//System.out.println(s);
			s = StringUtil.replaceBlank(s);  //删除所有不可见字符；
			if(s.equals("") || (s == null)){//跳过多余的；
				strCount--;
				continue;
			}
			String[] singleStrArray = new String[2];
			int i=0;
			int len = s.length();
			for(i=0;i<len; i++){
				char c = s.charAt(i);
				if(StringUtil.isChinese(c))
					break;
			}

			singleStrArray[0] = splitStr + s.substring(0, i);
			singleStrArray[1] = s.substring(i, len);

			strArray[j++] = singleStrArray;
		}
		if(strCount == 0){
			System.out.println("没有包含链接的文字");
		}else{
			System.out.println("有"+strCount+"段包含链接的文字：");
			for (int i = 0; i < strCount; i++) {
				for(String s : strArray[i])
					System.out.println(s);
			}
		}
	}

	public void findPictures(String path){
		ArrayList<String> picturesName = new ArrayList<String>();
		picCount = 0;
		File d = new File(path);
		//取得当前文件夹下所有文件和目录的列表
		File lists[] = d.listFiles();

		//对当前目录下面所有文件进行检索
		for(int i = 0; i < lists.length; i ++){

			if(lists[i].isFile()){
				String filename = lists[i].getName();
				String filetype = new String("");
				//取得文件类型
				filetype = filename.substring((filename.length() - 3), filename.length());

				//判断是否为所需图片；
				if(filename.startsWith(PIC_PART_NAME)){
					if(filetype.equals("jpg") || filetype.equals("png") || filetype.equals("gif")){	
						picturesName.add(PIC_PATH + "//" + filename);
						picCount++;
						System.out.println(filename + "已加入");
					}
				}
			}
		}//for;
		picArray = picturesName.toArray(new String[]{});
		System.out.println(picCount + "张图");
	}

	public static void main(String[] args) {
		String originalStr = "http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB07S4%2FK0CITy7klxxrJ35Nn 蜂蜜柚子茶 密炼柚子茶 韩国工艺 瓶装500克 全国包邮【14.80包邮 http://s.click.taobao.com/t?e=zGU34CA7K%2BPkqB07S4%2FK0CITy7klxxrJ35Nn 蜂蜜柚子茶 密炼柚子茶 韩国工艺 瓶装500克 全国包邮【14.80包邮";
		DataProcessor dp = new DataProcessor(originalStr);
		
		//System.out.println(dp.strCount);
	}
}
