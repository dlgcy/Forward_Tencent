package com.dlgcy.ui;

import javax.swing.BoxLayout;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class SettingFrame extends JFrame{
	
	public SettingFrame() {
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		
		p.add(new SettingPanel("发送时间间隔/秒", "nSecond", "10"));
		p.add(new SettingPanel("工作空间路径", "WORK_SPACE", "D://Forward//"));
		/*JFileChooser dirChooser = new JFileChooser();
		p.add(dirChooser);*/
		
		add(p);
		pack();
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(300, 200);
		setVisible(true);
	}
	
	/*public static void main(String[] args) {
		new SettingFrame();
	}*/
	
}
