package com.dlgcy.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.dlgcy.weibo.Forward;

public class AuthorizePanel extends JPanel{
	JTextArea hint1, hint2;
	JLabel picHint, inputHint;
	JTextField input;
	JButton sure;
	JPanel picPanel, inputPanel;
	ImageIcon img = new ImageIcon("image/picHint.png");
	
	public AuthorizePanel(){
		setSize(740,700);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		hint1 = new JTextArea("已在您的默认浏览器中打开授权页面，请在该页面进行授权，谢谢！");
		hint2 = new JTextArea("授权完成后请不要关闭跳转页面（百度首页），并请留意该页地址栏，以便获取相关验证信息。");
		
		picHint = new JLabel(img);
		
		
		inputPanel = new JPanel();
		inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.X_AXIS));
		inputHint = new JLabel("验证信息：");
		input = new JTextField();
		inputPanel.add(inputHint);
		inputPanel.add(input);
		sure = new JButton("确认");
		
		this.add(hint1);
		this.add(hint2);
		this.add(picHint);
		this.add(inputPanel);
		this.add(sure);
		
	}
	
}



