package com.dlgcy.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ShowItem extends JPanel{
	LinkLabel link;
	JLabel content, pic;
	
	public ShowItem(String linkStr, String contentStr, String picPath) {
		//JPanel p = new JPanel();
		
		link = new LinkLabel(linkStr, linkStr);
		content = new JLabel(contentStr);
		pic = new JLabel(new ImageIcon(picPath));
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(link);
		add(content);
		add(pic);
		add(Box.createVerticalStrut(8)); 
		
	}
	
	/*public static void main(String[] args) {
		JFrame f = new JFrame();
		ShowItem si = new ShowItem("www.baidu.com", "百度一下，你就知道", "image//winstart.jpg");
		
		f.add(si);
		f.pack();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}*/
	
}
