package com.dlgcy.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JButton;

public class RightButton extends JButton{

	public RightButton(String string) {
		super();
		this.setText(string);
		this.setPreferredSize(new Dimension(95, 30));
		setMargin(new Insets(5, 0, 5, 5));  //（无效）；
		setMaximumSize(getPreferredSize());   //没有这句大小就不变了；
	}
}
