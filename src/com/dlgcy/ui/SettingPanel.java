package com.dlgcy.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.dlgcy.weibo.ConfigUtil;

public class SettingPanel extends JPanel{
	JLabel hintL, hintR;
	JTextField value;
	JButton sure;
	
	public SettingPanel(String hints, final String key, final String origin) {
		hintL = new JLabel(hints+"("+key+"):");
		value = new JTextField();
		sure = new JButton("确认更改");
		hintR = new JLabel("原始值："+origin+" , 当前值："+ConfigUtil.getValue(key));
		
		//value.setPreferredSize(new Dimension(150, 20));
		value.setMaximumSize(new Dimension(200, 25)); 
		value.setMinimumSize(new Dimension(80, 25));
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		setPreferredSize(new Dimension(800, 35));
		add(Box.createHorizontalStrut(8)); 	
		add(hintL);
		add(Box.createHorizontalStrut(8)); 	
		add(value);
		add(Box.createHorizontalStrut(8)); 	
		add(sure);
		add(Box.createHorizontalStrut(8)); 	
		add(hintR);
		add(Box.createHorizontalStrut(8)); 	
		
		sure.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				String input = value.getText();
				
				if(input.equals("")){
					JOptionPane.showMessageDialog(sure, new JLabel("您未输入任何字符！"));
				}else{
					Object[] options = {"设置","取消"};
					int response = JOptionPane.showOptionDialog(sure, new JLabel("请慎重设置（建议备份配置文件）。确定要设置为该值吗？")
					,"确认设置", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
					if(response == 0){//用户确认；
						ConfigUtil.setValueAndSave(key, input, ConfigUtil.FILE_PATH);
						hintR.setText("原始值："+origin+" , 当前值："+ConfigUtil.getValue(key));
					}
				}
			}
		});
	}
	
	/*public static void main(String[] args) {
		JFrame f = new JFrame();
		SettingPanel sp = new SettingPanel("间隔", "nSecond", "2");
		
		f.add(sp);
		f.pack();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}*/
	
}
